<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
	</div>

	<div class="templateSectionMain">
		<div class="sectionWrapper">
			<div class="searchBox">
				<?php get_template_part('loop'); ?>

				<?php get_template_part('pagination'); ?>
			</div>
		</div>
	</div>

</div>
<?php get_footer(); ?>
