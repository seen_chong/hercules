<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-lease.jpg">
	</div>


			<?php
	  			$args = array(
	    		'post_type' => 'equipment-faq'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

	<div class="toggleBlock">
		<div class="toggleQuestion">
			<h4><?php the_field('question'); ?></h4>
		</div>
		<div class="toggleAnswer" style="display:none;">
			<p><?php the_field('answer'); ?></p>
		</div>
	</div>
			<?php
				}
					}
				else {
				echo 'No FAQs';
				}
			?>

</div>
	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>


</div> <!-- .siteWrapper -->

		

<?php get_footer(); ?>
