<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-events.jpg">
	</div>

	<div class="pageSectionOne">
		<div class="sectionWrapper">
			
		<div class="newsEventsBox">
			<h2>News</h2>

			<?php
	  			$args = array(
	    		'post_type' => 'upcoming-events'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

			<div class="upcomingEvent">
				<div class="dateBox">
					<div class="dateBorder cokeBorder">
						<span class="bigMonth coke"><?php the_field('month'); ?></span> <br />
						<span class="bigDate coke"><?php the_field('date'); ?></span>	
					</div>
					<span class="bigTime"><?php the_field('time'); ?></span>
				</div>
				<div class="eventDetails">
					<h3><?php the_field('event_name'); ?></h3>
					<p><?php the_field('description'); ?></p>

				<?php if (get_field('pdf_link') != ''): ?>
					<a href="<?php the_field('pdf_link'); ?>" target="blank">Click Here to Download</a>
				<?php endif; ?>
				
				</div>			
			</div>

			<?php
				}
					}
				else {
				echo 'No Upcoming Events';
				}
			?>
			</div>
		</div>		
	</div>
    
    <div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
		</div>
	</div>

</div> <!-- .siteWrapper -->	

<?php get_footer(); ?>
