			<!-- footer -->
			<footer class="siteFooter" role="contentinfo">

				<!-- copyright -->
				<p class="copyright pull-left">
					&copy; <?php echo date('Y'); ?> Hercules Corporation
				</p>
				<p class="address pull-right">
					550 West John Street &#8226; Hicksville &#8226; New York 11801
				</p>
				<!-- /copyright -->

			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		
	</body>
</html>

 <script type="text/javascript">
$(".toggleBlock").click(function(){
    $(".toggleAnswer", this).toggle();
});
</script>