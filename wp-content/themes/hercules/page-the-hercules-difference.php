<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-technology.jpg">
	</div>
		
<!-- 	<div class="heroBox">
		<h1><//?php the_title(); ?></h1>
	
		<div class="heroSubBox">
			<p><a href="">Our Family History Service ></a></p>
			<p><a href="">Leading-Edge Technology ></a></p>
			<p><a href="">Our Commitment to be Green ></a></p>
		</div>

		<h3>Think all Laundry Rooms are the same?</h3>
		<h4>Think Again</h4>
		
	</div> -->
		

	<div class="pageSectionOne">
		<div class="sectionWrapper">
			<h6><?php the_field('section_one_header'); ?></h6>
			<?php the_field('section_one_paragraph_1'); ?>
			<?php the_field('section_one_paragraph_2'); ?>
		</div>
		
	</div>
	<div class="pageSectionTwo">
		<div class="sectionWrapper">
			<p>It’s all part of the Hercules commitment to superior customer service that’s recognized by our industry and appreciated by the more than one million residents who use our laundry rooms.</p>
			<p>In fact, thanks to these customers, our company is a multiple recipient of the prestigious “Maytag Red Carpet Service Excellence Award” and the “Maytag Multi-Housing Excellence Award,” which recognizes outstanding service to the multi-housing market, including condominiums, co-ops, apartment buildings and colleges/universities.</p>
			<p>In 2010, our “green” commitment also earned Hercules the “Maytag Commercial Laundry Energy Advantage Award” for exceptional promotion and advocacy of energy and water efficiency.</p>

<!-- 			<p>It’s all part of the Hercules commitment to superior</p>
			<p>customer service that’s recognized by our industry and appreciated</p> 
			<p>by the more than one million residents who use our laundry rooms. In fact,</p> <p>thanks to these customers, our company is a multiple recipient of the prestigious “Maytag</p> 
			<p>Red Carpet Service Excellence Award” and the “Maytag Multi-Housing Excellence Award,”</p> 
			<p>which recognizes outstanding service to the multi-housing market, including condominiums, co-ops,</p> 
			<p>apartment buildings and colleges/universities. In 2010, our “green” commitment also earned Hercules the “Maytag</p> 
			<p>Commercial Laundry Energy Advantage Award” for exceptional promotion and advocacy of energy and water efficiency.</p> -->
		</div>
	</div>

	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

<?php get_footer(); ?>
