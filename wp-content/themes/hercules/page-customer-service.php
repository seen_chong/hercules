<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-customer-service.jpg">
	</div>

			<?php
	  			$args = array(
	    		'post_type' => 'service-faq'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

	<div class="toggleBlock">
		<div class="toggleQuestion">
			<h4><?php the_field('question'); ?></h4>
		</div>
		<div class="toggleAnswer" style="display:none;">
			<p><?php the_field('answer'); ?></p>
		</div>
	</div>

			<?php
				}
					}
				else {
				echo 'No FAQs';
				}
			?>


</div>

	</div>

</div> <!-- .siteWrapper -->

		

<?php get_footer(); ?>
