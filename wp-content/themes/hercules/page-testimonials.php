<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-installations.jpg">
	</div>

	<div class="contactBlock">

		<div class="contactBox">
			<div class="contactTrigger">
				<ul>
					<li class="triggerA">
						<a href="#">New York</a>
					</li>
					<li class="triggerB">
						<a href="#">New Jersey</a>
					</li>
					<li class="triggerD">
						<a href="#">Connecticut</a>
					</li>
					<li class="triggerC">
						<a href="#">Pennsylvania</a>
					</li>

				</ul>

				<div class="askAndrewBlock">
				<h5>Have A Question?</h5>
				<img src="<?php echo get_template_directory_uri(); ?>/img/ask_andrew.jpg">
				<p>Hercules President and CEO Andrew May will personally provide the answer. <br /> <a href="" class="askHere">Ask Here ></a></p>
				</div>
			</div>

			<div class="contactReveal">
				<div class="testNY">

			<?php
	  			$args = array(
	    		'post_type' => 'ny-testimonials'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>



					<div class="testBlock">
						<div class="testBlockHead">
							<a href="<?php the_permalink(); ?>">
								<h4><?php the_field('property'); ?></h4>
							</a>
							<h5><?php the_field('manager'); ?></h5>
						</div>
						<img src="<?php the_field('image'); ?>" alt="">
						<p><?php the_field('quote'); ?></p>
					</div>

							<?php
			}
				}
			else {
			echo 'No Testimonials Found';
			}
		?>
				
				</div>



				<div class="employeeList" style="display:none;">

			<?php
	  			$args = array(
	    		'post_type' => 'nj-testimonials'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>



					<div class="testBlock">
						<a href="<?php the_permalink(); ?>">
							<h4><?php the_field('property'); ?></h4>
						</a>
						<h5><?php the_field('manager'); ?></h5>
						<img src="<?php the_field('image'); ?>" alt="">
						<p><?php the_field('quote'); ?></p>
					</div>

							<?php
			}
				}
			else {
			echo 'No Testimonials Found';
			}
		?>

				</div>




				<div class="careerPost" style="display:none;">

			<?php
	  			$args = array(
	    		'post_type' => 'pa-testimonials'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>



					<div class="testBlock">
						<a href="<?php the_permalink(); ?>">
							<h4><?php the_field('property'); ?></h4>
						</a>
						<h5><?php the_field('manager'); ?></h5>
						<img src="<?php the_field('image'); ?>" alt="">
						<p><?php the_field('quote'); ?></p>
					</div>

							<?php
			}
				}
			else {
			echo 'No Testimonials Found';
			}
		?>

				</div>


				<div class="testCT" style="display:none;">

			<?php
	  			$args = array(
	    		'post_type' => 'ct-testimonials'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>



					<div class="testBlock">
						<a href="<?php the_permalink(); ?>">
							<h4><?php the_field('property'); ?></h4>
						</a>
						<h5><?php the_field('manager'); ?></h5>
						<img src="<?php the_field('image'); ?>" alt="">
						<p><?php the_field('quote'); ?></p>
					</div>

							<?php
			}
				}
			else {
			echo 'No Testimonials Found';
			}
		?>

				</div>
				
			</div>
		</div>
	</div>




	<div class="contactSectionBlock">
		<div class="sectionWrapper">
		</div>		
	</div>
    
    	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

		

<?php get_footer(); ?>


<script type="text/javascript">
	$('.triggerA').click(function() {
		$('.testNY').show();
		$('.employeeList').hide();
		$('.careerPost').hide();
		$('.testCT').hide();
	});

	$('.triggerB').click(function() {
		$('.employeeList').show();
		$('.testNY').hide();
		$('.careerPost').hide();
		$('.testCT').hide();
	});

	$('.triggerC').click(function() {
		$('.careerPost').show();
		$('.employeeList').hide();
		$('.testNY').hide();
		$('.testCT').hide();
	});

	$('.triggerD').click(function() {
		$('.testCT').show();
		$('.careerPost').hide();
		$('.employeeList').hide();
		$('.testNY').hide();
	});
</script>