<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		

				<script type="text/javascript">
			$(document).ready(function(){
			  $('selector').slippry()
			});
		</script>

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="siteWrapper">

			<!-- header -->
			<header class="header clear" role="banner">

				<div class="headerTopWrapper">

					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/hercules-logo.png" alt="Logo" class="logoImg">
						</a>
					</div>

					<div class="headerLeft">
						<p>
							<span id="tollFree">Toll-Free #</span> <br />
							<span id="tollNumber">1-800-526-5760</span>
						</p>
					</div>

					<div class="headerRight">
						<!-- Search Field -->
						<?php echo do_shortcode("[wpbsearch]"); ?>
						<a href="/product/smart-card-reload/">
							<input type="button" name="revalue" value="Revalue Smart Card" class="revalueBtn">
						</a>
						<a href="/contact-us">
							<input type="button" name="contactUs" value="Contact Us" class="contactBtn">
						</a>
					</div>

				</div>

<!-- mobile-menu -->
				<div class="mobileTopWrapper" >

					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/hercules-logo.png" alt="Logo" class="logoImg">
						</a>
					</div>


					<div class="headerRight">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/img/mobile_phone.png" alt="Logo" class="phoneImg">
						</a>
					</div>

				</div>
<!-- end mobile-menu -->
                
				<div class="headerBottomWrapper">

					<!-- nav -->
					<nav class="nav" role="navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
					</nav>
					<!-- /nav -->
                 
<!-- This div is used to indicate the original position of the scrollable fixed div. -->
                    <div class="scrollerAnchor"></div>
<!-- This div will be displayed as fixed bar at the top of the page, when user scrolls --> 
                    <div class="scroller">
                    	<a href="/customer-service/service-request/">
                        	<input type="button" name="requestService" value="Request Service" class="requestServiceBtn">
                        </a>
                        <a href="/equipment-lease-sales/request-a-proposal/">
                        	<input type="button" name="proposal" value="Get A Proposal" class="proposalBtn">
                        </a>
                    </div>
				</div>

			</header>
			<!-- /header -->
            