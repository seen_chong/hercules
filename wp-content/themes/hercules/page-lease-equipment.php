<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-lease.jpg">
	</div>
	<div class="templateSectionMain">
		<div class="sectionWrapper">
			<h6>Hercules provides end-to-end laundry room equipment lease packages that combine top-rated, energy-efficient models from Maytag, Wascomat and American Dryer, superior 24/7 customer service and leading-edge technology to meet the needs of the multi-housing markets in New York, New Jersey and Connecticut and Pennsylvania.</h6>


			<img src="http://hercnet.com/images/uploads/ADC.gif" style="margin-top:50px;">

			<?php
	  			$args = array(
	    		'post_type' => 'american-dryer'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

				<div class="templateSectionMain">
					<div class="sectionWrapper singleTestWrapper">
						<img class="leaseImg" src="<?php the_field('image'); ?>" alt="">
						<h6 class="propText"><?php the_field('name'); ?></h6>
						<?php the_field('text'); ?>
						<a class="manual-pdf" href="<?php the_field('pdf'); ?>">Manual Download (PDF)</a>
					</div>
				</div>
			

		<?php
						}
				}
			else {
			echo 'No Equipment Found';
			}
		?>

			<img src="http://hercnet.com/images/uploads/logo-maytag.png" style="margin-top:50px;">
			<p>Maytag Commercial Energy Advantage products help you cut costs and maximize profits. Our TurboWash system offers outstanding cleaning performance. And, it uses specially designed spray baffles and tumble patterns that provide more cleaning power while using less water.</p>

						<?php
	  			$args = array(
	    		'post_type' => 'maytag'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

				<div class="templateSectionMain">
					<div class="sectionWrapper singleTestWrapper">
						<img class="leaseImg" src="<?php the_field('image'); ?>" alt="">
						<h6 class="propText"><?php the_field('name'); ?></h6>
						<?php the_field('text'); ?>
						<a class="manual-pdf" href="<?php the_field('pdf'); ?>">Manual Download (PDF)</a>
					</div>
				</div>
			

		<?php
						}
				}
			else {
			echo 'No Equipment Found';
			}
		?>

			<img src="http://hercnet.com/images/uploads/Wascomat.jpg" style="margin-top:50px;">
			<p>The Crossover has 4 user friendly wash programs, with 2 additional options designed to save the owner energy costs and deliver the best customer experience in the industry. It is engineered exclusively for commercial use. Commercial washers are designed for 15,000 +cycles-more than double the life of appliance-type washers made for homes. All Crossover machines are soft-mount for easy installation.  They are built tough with top quality SKF bearings, triple seals, solid steel counterweights, and 8-point suspension system.</p>

			<?php
	  			$args = array(
	    		'post_type' => 'wascomat'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

				<div class="templateSectionMain">
					<div class="sectionWrapper singleTestWrapper">
						<img class="leaseImg" src="<?php the_field('image'); ?>" alt="">
						<h6 class="propText"><?php the_field('name'); ?></h6>
						<?php the_field('text'); ?>
						<a class="manual-pdf" href="<?php the_field('pdf'); ?>">Manual Download (PDF)</a>
					</div>
				</div>
			

		<?php
						}
				}
			else {
			echo 'No Equipment Found';
			}
		?>

		</div>
	</div>

	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

<?php get_footer(); ?>

<style type="text/css">
	img {
		text-align: center;
		display: block;
		margin: 0 auto;
	}
</style>