<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-customer-service.jpg">
	</div>


	<div class="contactBlock">

		<div class="contactBox">
			<div class="contactTrigger">
				<ul>
					<li class="triggerA">
						<a href="#">Service Request</a>
					</li>
				</ul>

				<div class="askAndrewBlock">
				<h5>Have A Question?</h5>
				<img src="<?php echo get_template_directory_uri(); ?>/img/ask_andrew.jpg">
				<p>Hercules President and CEO Andrew May will personally provide the answer. <br /> <a href="/ask-andrew" class="askHere">Ask Here ></a></p>
				</div>
			</div>

			<div class="contactReveal">
				<div class="contactForm">
					<h4>Service Request</h4>
					<ul>
						<li>For service requests or problems please call us at 1-800-526-5760 or fill out the online form.</li>
						<li>To request a proposal, please call us at 1-800-526-5760 or fill out the online form.</li>
						<li>Do you have a question, comment or suggestion if so call us at 1-800-526-5760. To send us a comment, fill out the following form:</li>
					</ul>

					<?php echo do_shortcode('[contact-form-7 id="204" title="Service Request"]'); ?>
				</div>
				
				</div>

			</div>
		</div>
	</div>




	<div class="contactSectionBlock">
		<div class="sectionWrapper">
		</div>		
	</div>
    
    	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

		

<?php get_footer(); ?>


<script type="text/javascript">
	$('.triggerA').hover(function() {
		$('.testNY').show();
		$('.employeeList').hide();
		$('.careerPost').hide();
	});

	$('.triggerB').hover(function() {
		$('.employeeList').show();
		$('.testNY').hide();
		$('.careerPost').hide();
	});

	$('.triggerC').hover(function() {
		$('.careerPost').show();
		$('.employeeList').hide();
		$('.testNY').hide();
	})
</script>