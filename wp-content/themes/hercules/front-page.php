<?php get_header(); ?>

<div class="slider">	
	<?php echo do_shortcode("[masterslider id=1]"); ?>
	<h1>The Smart Choice In Laundry</h1>
</div>

<div class="pageWrapper">

	<div class="serviceBoxes">

		<div class="requestServiceBox">
			<div class="requestServiceTop">
				<a href="/customer-service/service-request/">
					<h2>Request Service <i class="flaticon-fastforward4"></i> </h2>
				</a>
				<p>A service technician will be there within 24 hours-guaranteed.</p>			
				<img class="rotateCog" src="<?php echo get_template_directory_uri(); ?>/img/cog_icon.png">
				
			</div>
			<div class="requestServiceBottom">
				<p>On-Demand &#8226; Right on Time &#8226; Reliable</p>
			</div>
		</div>

		<div class="moneyCardBox">
			<div class="moneyCardTop">
				<h2>Add Money to your Card</h2>
			</div>
			<div class="moneyCardBottom">
				<p>Select which card you would like to add money to:</p>
				<a href="/product/smart-card-reload/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/hercules_card_1.png">
				</a>
				<a href="/product/smart-card-reload/">
					<img src="<?php echo get_template_directory_uri(); ?>/img/hercules_card_2.png">
				</a>
			</div>
		</div>

	</div>

	<div class="whatWeDoBoxes">
		<div class="sectionTitle">
			<h1>What We Do</h1>
		</div>

		<?php
	  			$args = array(
	    		'post_type' => 'whatwedo',
	    		'posts_per_page' => 3 
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
		
		<div class="wwdBox">
			<h2><i class="<?php the_field('icon'); ?>"></i> <?php the_field('service'); ?></h2>
			<img src="<?php the_field('image'); ?>">
			<p><?php the_field('short_description'); ?></p>
			<a href="<?php the_field('link'); ?>" class="learnMore">Learn More ></a>
		</div>

		<?php
			}
				}
			else {
			echo 'No Services Found';
			}
		?>


	</div>
</div>

	<div class="newsletterBox">
		<h1>Join our newsletter to stay in the loop with:</h1>
		<input type="email" name="newsletter" placeholder="your@email.com" class="newsletterEmail">
		<input type="submit" name="submit" value="Stay Updated" class="updateBtn">

		<ul class="newsletterBullets">
			<li>upcoming events</li>
			<li>news installations</li>
			<li>lorem ipsum</li>
		</ul>
	</div>

	<div class="tidbitsSection">

		<div class="questionBox">
			<h1>Have a Question?</h1>
			<div class="askAndrew">
				<img src="<?php echo get_template_directory_uri(); ?>/img/ask_andrew.jpg">
				<p>Hercules President and CEO Andrew May will personally provide the answer. <br /> <a href="/ask-andrew/" class="askHere">Ask Here ></a></p>
			</div>
		</div>

		<div class="eventsBox">
			<h2>Upcoming Events</h2>

			<?php
	  			$args = array(
	    		'post_type' => 'upcoming-events',
	    		'posts_per_page' => 2 
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

			<div class="upcomingEvent">
				<div class="dateBox">
					<div class="dateBorder mantisBorder">
						<span class="month mantis"><?php the_field('month'); ?></span> <br />
						<span class="date mantis"><?php the_field('date'); ?></span>	
					</div>
					<span class="time"><?php the_field('time'); ?></span>
				</div>
				<div class="eventDetails">
					<h3><?php the_field('event_name'); ?></h3>
					<p><?php the_field('description'); ?></p>
				</div>			
			</div>

			<?php
				}
					}
				else {
				echo 'No Upcoming Events';
				}
			?>

		</div>

		<div class="testimonialsBox">
			<h3>Testimonials</h3>	
			<?php
	  			$args = array(
	    		'post_type' => 'testimonials',
	    		'posts_per_page' => 1 
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
			<p><?php the_field('testimonial'); ?> <a href="/installations/testimonials/" id="readMore"> [Read More]</a></p>
			<?php
				}
					}
				else {
				echo 'No Testimonials';
				}
			?>
		</div>

			

	</div>

</div>

<?php get_footer(); ?>
