<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-history.jpg">
	</div>
	<div class="templateSectionMain">
		<div class="sectionWrapper singleTestWrapper singlePostWrapper">
			<a href="/contact-us/">
				<button class="backPost">Back To Job Postings</button>
			</a>

			<p><strong>Date Posted:</strong> <?php the_field('post_date'); ?></p>
			<p><strong>Job Type:</strong> <?php the_field('position'); ?></p>
			<p><strong>Location:</strong> <?php the_field('location'); ?></p>
			<br>

			<p><strong>Contact Information</strong></p>
			<p><strong>Name:</strong> Barry Heller</p>
			<p><strong>Phone:</strong> 516-281-8754</p>
			<p><strong>Email:</strong> barry.heller@hercnet.com</p>
			<p><strong>Address:</strong> 550 West John Street, Hicksville ,11801</p>
			<br>

			<p>We are currently seeking self-motivated Field Service Technicians to fill immediate positions at Hercules. The Field Service Technician is responsible for the day-to-day maintenance and repair of commercial laundry equipment (washers and dryers) in NY, NJ, CT and PA. The position provides the opportunity for personal growth through experience and training.</p>
			<br>

			<p><strong>Responsibilities:</strong></p>
			<?php the_field('responsibilities'); ?>
			<br>

			<p><strong>Requirements:</strong></p>
			<?php the_field('requirements'); ?>
			<br>

			<p>We offer competitive pay and a full benefits package including medical/dental and paid time off. All necessary training will also be provided to perform all required work.</p>
		</div>
	</div>

	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

<?php get_footer(); ?>
