<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-customer-service.jpg">
	</div>


	<div class="contactBlock">

		<div class="contactBox">
			<div class="contactTrigger">
				<ul>
					<li class="triggerA">
						<a href="#">Request a Smart Card</a>
					</li>
				</ul>

				<div class="askAndrewBlock">
				<h5>Have A Question?</h5>
				<img src="<?php echo get_template_directory_uri(); ?>/img/ask_andrew.jpg">
				<p>Hercules President and CEO Andrew May will personally provide the answer. <br /> <a href="/ask-andrew/" class="askHere">Ask Here ></a></p>
				</div>
			</div>

			<div class="contactReveal">
				<div class="contactForm">
					<h4>Request a Smart Card</h4>
					<ul>
						<li>If you are a new resident that requires a Smart Card or an existing resident who requires a replacement card, please fill out the form below to request a new card. Your Smart Card will arrive in 3-5 business days. Smart Cards cannot be mailed to P.O. boxes.</li>
					</ul>

					<?php echo do_shortcode('[contact-form-7 id="213" title="Smart Card Request"]'); ?>
				</div>
				
				</div>

			</div>
		</div>
	</div>




	<div class="contactSectionBlock">
		<div class="sectionWrapper">
		</div>		
	</div>
    
    	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

		

<?php get_footer(); ?>


<script type="text/javascript">
	$('.triggerA').hover(function() {
		$('.testNY').show();
		$('.employeeList').hide();
		$('.careerPost').hide();
	});

	$('.triggerB').hover(function() {
		$('.employeeList').show();
		$('.testNY').hide();
		$('.careerPost').hide();
	});

	$('.triggerC').hover(function() {
		$('.careerPost').show();
		$('.employeeList').hide();
		$('.testNY').hide();
	})
</script>