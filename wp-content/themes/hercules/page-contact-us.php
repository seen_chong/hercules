<?php get_header(); ?>

	<div class="pageHero">	
		<div class="iframeHack">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3021.817929791681!2d-73.55011948459335!3d40.76602907932603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c280d0a920b6fd%3A0xe1d471ae38f312a!2s550+W+John+St%2C+Hicksville%2C+NY+11801!5e0!3m2!1sen!2sus!4v1462396266317" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

		
		<div class="heroBox">
			<h1 class="contact-us-h1"><?php the_title(); ?></h1>		
		</div>	
	</div>

	<div class="contactBlock">

		<div class="contactBox">
			<div class="contactTrigger">
				<ul>
					<li class="triggerA">
						<a href="#">Contact Us</a>
					</li>
					<li class="triggerB">
						<a href="#">Employee Directory</a>
					</li>
					<li class="triggerC">
						<a href="#">Career Opportunities</a>
					</li>
				</ul>

				<div class="askAndrewBlock">
				<h5>Have A Question?</h5>
				<img src="<?php echo get_template_directory_uri(); ?>/img/ask_andrew.jpg">
				<p>Hercules President and CEO Andrew May will personally provide the answer. <br /> <a href="/ask-andrew" class="askHere">Ask Here ></a></p>
				</div>
			</div>

			<div class="contactReveal">
				<div class="contactForm">
					<h4>Hercules prides itself on providing exceptional customer service and support.</h4>
					<ul>
						<li>For service requests or problems please call us at 1-800-526-5760 or fill out the online form.</li>
						<li>To request a proposal, please call us at 1-800-526-5760 or fill out the online form.</li>
						<li>Do you have a question, comment or suggestion if so call us at 1-800-526-5760. To send us a comment, fill out the following form:</li>
					</ul>

					<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
				</div>
				<div class="employeeList" style="display:none;">
					<h4>Executives</h4>
					<ul>
						<li>
							<p>Al May <br> 
								<a href="mailto:executives@hercnet.com?Subject=Contact">executives@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Andrew May <br> 
								<a href="mailto:executives@hercnet.com?Subject=Contact">executives@hercnet.com</a>
							</p>
						</li>
					</ul>

					<h4>Finance</h4>
					<ul>
						<li>
							<p>Matthew Reich <br> 
								<a href="mailto:matthew.reich@hercnet.com?Subject=Contact">matthew.reich@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Jeannine Redding <br> 
								<a href="mailto:jeannine.redding@hercnet.com?Subject=Contact">jeannine.redding@hercnet.com</a>
							</p>
						</li>
					</ul>

					<h4>Marketing</h4>
					<ul>
						<li>
							<p>Barry Heller <br> 
								<a href="mailto:barry.heller@hercnet.com?Subject=Contact">barry.heller@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Adam May <br> 
								<a href="mailto:adam.may@hercnet.com?Subject=Contact">adam.may@hercnet.com</a>
							</p>
						</li>
					</ul>

					<h4>Sales</h4>
					<ul>
						<li>
							<p>Mark Eisler <br> 
								<a href="mailto:mark.eisler@hercnet.com?Subject=Contact">mark.eisler@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>​Dan Horowitz <br> 
								<a href="mailto:dan.horowitz@hercnet.com?Subject=Contact">​dan.horowitz@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>John Abraham <br> 
								<a href="mailto:john.abraham@hercnet.com?Subject=Contact">john.abraham@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Jay Osman <br> 
								<a href="mailto:jay.osman@hercnet.com?Subject=Contact">jay.osman@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Tom Jeppestol <br> 
								<a href="mailto:tom.jeppestol@hercnet.com?Subject=Contact">tom.jeppestol@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Mark Lubin <br> 
								<a href="mailto:mark.lubin@hercnet.com?Subject=Contact">mark.lubin@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Andy Tegan <br> 
								<a href="mailto:andy.tegan@hercnet.com?Subject=Contact">andy.tegan@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Christine Franke <br> 
								<a href="mailto:christine.franke@hercnet.com?Subject=Contact">christine.franke@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Daniel Horowitz <br> 
								<a href="mailto:executives@hercnet.com?Subject=Contact">daniel.horowitz@hercnet.com</a>
							</p>
						</li>
					</ul>

					<h4>Service & Operations</h4>
					<ul>
						<li>
							<p>Steve Beckerman <br> 
								<a href="mailto:steve.beckerman@hercnet.com?Subject=Contact">steve.beckerman@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Neal Melandovich <br> 
								<a href="mailto:neal.melandovich@hercnet.com?Subject=Contact">neal.melandovich@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>Paul Finkelstein <br> 
								<a href="mailto:paul.finkelstein@hercnet.com?Subject=Contact">paul.finkelstein@hercnet.com</a>
							</p>
						</li>
						<li>
							<p>​George Ledson <br> 
								<a href="mailto:​george.ledson@hercnet.com?Subject=Contact">​george.ledson@hercnet.com</a>
							</p>
						</li>
					</ul>
				</div>

				<div class="careerPost" style="display:none;">
					<h4>Available Positions</h4>
					<table class="jobPosting">
					  <tr class="postingHeader">
					    <th>Job Title</a></th>
					    <th>Type</th> 
					    <th>Post Date</th>
					    <th>Location</th>
					  </tr>

			<?php
	  			$args = array(
	    		'post_type' => 'job-postings'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

					  <tr>
					    <td>
					    	<a href="<?php the_permalink(); ?>"><?php the_field('job_title'); ?></a>
					    	</td>
					    <td><?php the_field('type'); ?></td> 
					    <td><?php the_field('post_date'); ?></td>
					    <td><?php the_field('location'); ?></td>
					  </tr>

			<?php
				}
					}
				else {
				echo 'No Testimonials Found';
				}
			?>

					</table>
				</div>

			</div>
		</div>
	</div>




	<div class="contactSectionBlock">
		<div class="sectionWrapper">
		</div>		
	</div>
    
    	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

		

<?php get_footer(); ?>


<script type="text/javascript">
	$('.triggerA').hover(function() {
		$('.contactForm').show();
		$('.employeeList').hide();
		$('.careerPost').hide();
	});

	$('.triggerB').hover(function() {
		$('.employeeList').show();
		$('.contactForm').hide();
		$('.careerPost').hide();
	});

	$('.triggerC').hover(function() {
		$('.careerPost').show();
		$('.employeeList').hide();
		$('.contactForm').hide();
	})
</script>