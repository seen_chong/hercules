<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-history.jpg">
	</div>
	<div class="templateSectionMain">
		<div class="sectionWrapper singleTestWrapper">
			<a href="/customer-service/how-to-library/">
				<button class="backTest">Back To How-To Library</button>
			</a>
			<?php the_field('video'); ?>
		</div>
	</div>

	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

<?php get_footer(); ?>
