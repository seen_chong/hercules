<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-events.jpg">
	</div>
	<div class="templateSectionMain">
		<div class="sectionWrapper">
			<h6><?php the_field('bold_text'); ?></h6>
			<?php the_field('content'); ?>
			<?php
	  			$args = array(
	    		'post_type' => 'smart-card-machines'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

				<div class="templateSectionMain">
					<div class="sectionWrapper singleTestWrapper">
						<img class="leaseImg" src="<?php the_field('image'); ?>" alt="">
						<h6 class="propText"><?php the_field('title'); ?></h6>
						<p><?php the_field('text'); ?></p>
						<a class="manual-pdf" href="<?php the_field('pdf'); ?>">Download Instructions</a>
						<br>
						<a class="manual-pdf" href="<?php the_permalink(); ?>">Watch "How-To" Video</a>
					</div>
				</div>
			

		<?php
						}
				}
			else {
			echo 'No Equipment Found';
			}
		?>


		</div>
	</div>

	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

<?php get_footer(); ?>

<style type="text/css">
	img {
		text-align: center;
		display: block;
		margin: 0 auto;
	}
</style>