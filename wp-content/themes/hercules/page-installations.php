<?php get_header(); ?>

	<div class="templateSectionOne">
		<h1><?php the_title(); ?></h1>
		<img class="templateBanner" src="<?php echo get_template_directory_uri(); ?>/img/header-installations.jpg">
	</div>

	<div class="pageSectionOne">
		<div class="sectionWrapper">
			<h6><?php the_field('bold_text'); ?></h6>
			<?php the_field('content'); ?>
		</div>		
	</div>

	<div class="toggleBlock">
		<div class="sectionWrapper">
			<h4>Phase 1: Getting Started</h4>
		</div>
		<div class="toggleAnswer" style="display:none;">
			<?php the_field('phase_1'); ?>
		</div>
	</div>

	<div class="toggleBlock">
		<div class="sectionWrapper">
			<h4>Phase 2: Room Design</h4>
		</div>
		<div class="toggleAnswer" style="display:none;">
			<?php the_field('phase_2'); ?>
		</div>
	</div>
	<div class="toggleBlock">
		<div class="sectionWrapper">
			<h4>Phase 3: Contractor Coordination & Installation</h4>
		</div>
		<div class="toggleAnswer" style="display:none;">
			<?php the_field('phase_3'); ?>
		</div>
	</div>
	<div class="toggleBlock">
		<div class="sectionWrapper">
			<h4>Phase 4: Resident Relations</h4>
		</div>
		<div class="toggleAnswer" style="display:none;">
			<?php the_field('phase_4'); ?>
		</div>
	</div>
    
    	<div class="pageSectionBottom">
		<div class="sectionWrapper">
			<div class="requestProposalText">
				<h3>See what a difference the Hercules Difference can make in your laundry room</h3>
			</div>
			<div class="requestProposalAction">
				<a href="/equipment-lease-sales/request-a-proposal/">
					<input type="button" name="proposal" value="Request A Proposal" class="reqProposalBtn">
				</a>
			</div>
			
		</div>
	</div>

</div> <!-- .siteWrapper -->

		

<?php get_footer(); ?>
