<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hercules_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/or<&=R@>@+V8uR,gUo4j={PS&{@|3G08v<k+Y-MbO&|F6]8W!vnFN1B]RXm)+Z|');
define('SECURE_AUTH_KEY',  'y#JK#85CF[!S U}]$-Uvxet_0.#.v8bruWDvr}7g;_z2Kfl$IWy%IQ[QW&QvI1;Z');
define('LOGGED_IN_KEY',    'P?-x6 8tp|)_<3ed8Pl=`Z2k-E<&|94WU|F<s#g{QQQ2crUbL?*+_ysIQ9qxddeL');
define('NONCE_KEY',        'yzxlVh0.`}Zv49|5{8q:LeRD)<Wj1U&0+@8a.ZnB[~>5OAWDU#xD)Yz/f--L+KMl');
define('AUTH_SALT',        ',^~3PXY;)qCu||Z#-3#X;2Hg5N@65D*3Lt9T&~OtE?G3h:9eL4Ri69#|{M!M4QTe');
define('SECURE_AUTH_SALT', 'B%1rPl^v-J(=-a_$P>M>)/H9{R;JS359Dnx%wB!Ce.x5ELX E(cPJ6Wp|6pX8AD7');
define('LOGGED_IN_SALT',   '/>N4RVdb@8Oa}}wMn6hAY{]|N2L~j`j30v`cbJ.T|pHJ`(j:Cumyh~ze>U:?jST}');
define('NONCE_SALT',       '>s:Ya!=<_:3mcFG|tGX(qYxa~}i68 G)zj%Xj|9-GK#a7qT|kHRo).X+&v#G=ZRw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
